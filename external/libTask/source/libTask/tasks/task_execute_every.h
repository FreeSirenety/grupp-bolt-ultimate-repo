/*
Copyright (C) 2015  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef EXECUTE_EVERY_H
#define EXECUTE_EVERY_H
#include <chrono>
#include "../task.h"
#include "../task_manager.h"


class Task_execute_every : public Task
{
public:
	Task_execute_every(std::chrono::milliseconds milliseconds, Task* t, bool high_priority = false);
	void execute(Task_manager* manager);
	void* result();
private:
	std::chrono::milliseconds milliseconds;
	Task* t;
	bool high_priority;
};

#endif // EXECUTE_EVERY_H
