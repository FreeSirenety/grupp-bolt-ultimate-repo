/*
Copyright (C) 2015  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <stdio.h>
#include "task_print.h"
#include "../task.h"
#include "../task_manager.h"

Task_print::Task_print(const char *name) : Task(name)
{
}

Task_print::~Task_print()
{
	printf("Deleting print task.\r\n");
}

void Task_print::execute(Task_manager* manager)
{
	printf("%s\r\n", get_name());
}

void* Task_print::result()
{
	return 0;
}
