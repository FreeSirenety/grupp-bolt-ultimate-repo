// Frustum

#pragma once

#include "GameComponent.h"

namespace Library
{
	struct Plane
	{
		float m_D = 0;

		XMFLOAT3 m_normal;
	};

	class Frustum : public GameComponent
	{
		RTTI_DECLARATIONS(Frustum, GameComponent)

	public:
		Frustum(Game& game);

		void construct(XMMATRIX* viewproj);
		bool IsInside(XMFLOAT3* position, float radious);

		Plane m_Planes[6];
	};
}