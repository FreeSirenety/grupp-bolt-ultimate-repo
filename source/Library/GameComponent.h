#pragma once

#include "Common.h"

namespace Library
{
    class Game;
    class GameTime;

    class GameComponent : public RTTI
    {
        RTTI_DECLARATIONS(GameComponent, RTTI)

	public:

		enum EObjectType
		{
			Player,
			Enemy,
			Standard,
			Nondrawable,
			Grid,
			Skybox,
			Bullet
		};

    public:
        GameComponent();
        GameComponent(Game& game);
        virtual ~GameComponent();

        Game* GetGame();
        void SetGame(Game& game);
        bool Enabled() const;
        void SetEnabled(bool enabled);

        virtual void Initialize();
        virtual void Update(const GameTime& gameTime);

		void SetObjectType(EObjectType objectType);
		EObjectType GetObjectType();

		void TriggerDestroy();
		bool GetDestroyTriggered();

    protected:
        Game* mGame;
        bool mEnabled;

		EObjectType mObjectType;

		bool mDestroyTriggered;

    private:
        GameComponent(const GameComponent& rhs);
        GameComponent& operator=(const GameComponent& rhs);
    };
}
