// Frustum.cpp

#include "Frustum.h"
#include <math.h>

namespace Library
{
	RTTI_DEFINITIONS(Frustum)

	Frustum::Frustum(Game& game)
		: GameComponent(game)
	{

	}

	void Frustum::construct(XMMATRIX* viewproj)
	{
		XMVECTOR FirstVector = viewproj->r[0];
		XMVECTOR SecondVector = viewproj->r[1];
		XMVECTOR ThirdVector = viewproj->r[2];
		XMVECTOR FourthVector = viewproj->r[3];

		// left
		m_Planes[0].m_normal.x = XMVectorGetW(FirstVector) + XMVectorGetX(FirstVector);
		m_Planes[0].m_normal.y = XMVectorGetW(SecondVector) + XMVectorGetX(SecondVector);
		m_Planes[0].m_normal.z = XMVectorGetW(ThirdVector) + XMVectorGetX(ThirdVector);
		m_Planes[0].m_D = XMVectorGetW(FourthVector) + XMVectorGetX(FourthVector);

		// right
		m_Planes[1].m_normal.x = XMVectorGetW(FirstVector) - XMVectorGetX(FirstVector);
		m_Planes[1].m_normal.y = XMVectorGetW(SecondVector) - XMVectorGetX(SecondVector);
		m_Planes[1].m_normal.z = XMVectorGetW(ThirdVector) - XMVectorGetX(ThirdVector);
		m_Planes[1].m_D = XMVectorGetW(FourthVector) - XMVectorGetX(FourthVector);

		// top
		m_Planes[2].m_normal.x = XMVectorGetW(FirstVector) - XMVectorGetY(FirstVector);
		m_Planes[2].m_normal.y = XMVectorGetW(SecondVector) - XMVectorGetY(SecondVector);
		m_Planes[2].m_normal.z = XMVectorGetW(ThirdVector) - XMVectorGetY(ThirdVector);
		m_Planes[2].m_D = XMVectorGetW(FourthVector) - XMVectorGetY(FourthVector);

		// bottom
		m_Planes[3].m_normal.x = XMVectorGetW(FirstVector) + XMVectorGetY(FirstVector);
		m_Planes[3].m_normal.y = XMVectorGetW(SecondVector) + XMVectorGetY(SecondVector);
		m_Planes[3].m_normal.z = XMVectorGetW(ThirdVector) + XMVectorGetY(ThirdVector);
		m_Planes[3].m_D = XMVectorGetW(FourthVector) + XMVectorGetY(FourthVector);

		// near
		m_Planes[4].m_normal.x = XMVectorGetZ(FirstVector);
		m_Planes[4].m_normal.y = XMVectorGetZ(SecondVector);
		m_Planes[4].m_normal.z = XMVectorGetZ(ThirdVector);
		m_Planes[4].m_D = XMVectorGetZ(FourthVector);

		// far
		m_Planes[5].m_normal.x = XMVectorGetW(FirstVector) - XMVectorGetZ(FirstVector);
		m_Planes[5].m_normal.y = XMVectorGetW(SecondVector) - XMVectorGetZ(SecondVector);
		m_Planes[5].m_normal.z = XMVectorGetW(ThirdVector) - XMVectorGetZ(ThirdVector);
		m_Planes[5].m_D = XMVectorGetW(FourthVector) - XMVectorGetZ(FourthVector);

		for (auto plane : m_Planes)
		{
			float planeNormalLength = sqrtf(plane.m_normal.x * plane.m_normal.x + plane.m_normal.y * plane.m_normal.y + plane.m_normal.z * plane.m_normal.z);

			plane.m_normal.x = plane.m_normal.x / planeNormalLength;
			plane.m_normal.y = plane.m_normal.y / planeNormalLength;
			plane.m_normal.z = plane.m_normal.z / planeNormalLength;

			plane.m_D /= planeNormalLength;
		}
	}

	bool Frustum::IsInside(XMFLOAT3 *position, float radius)
	{
		for (auto plane : m_Planes)
		{
			XMVECTOR PlaneNormal = XMLoadFloat3(&plane.m_normal);
			XMVECTOR OtherPosition = XMLoadFloat3(position);

			float xDot = XMVectorGetX(PlaneNormal) * XMVectorGetX(OtherPosition);
			float yDot = XMVectorGetY(PlaneNormal) * XMVectorGetY(OtherPosition);
			float zDot = XMVectorGetZ(PlaneNormal) * XMVectorGetZ(OtherPosition);
			
			radius = 3.f;

			if (xDot + yDot + zDot + plane.m_D < -radius)
			{
				return false;
			}
		}

		return true;
	}
}