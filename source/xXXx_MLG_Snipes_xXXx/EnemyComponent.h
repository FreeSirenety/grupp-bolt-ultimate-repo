#pragma once

#include "DrawableGameComponent.h"
#include "SoundManager.h"
#include "StateManager.h"
#include "PlayerComponent.h"

using namespace Library;

namespace Library
{
	class Effect;
	class PointLight;
	class ProxyModel;
	class RenderStateHelper;
	class Keyboard;
	class Mouse;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class EnemyMaterial;
	class PlayerCamera;

	class EnemyComponent : public GameObjectComponent
	{
		RTTI_DECLARATIONS(EnemyComponent, GameObjectComponent)

	public:
		EnemyComponent(Game& game, Camera& camera);
		~EnemyComponent();

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime, bool KeepSettings = false) override;

		void SetAITarget();

		virtual Material* CreateMaterial() override;
	private:
		EnemyComponent();
		EnemyComponent(const EnemyComponent& rhs);
		EnemyComponent& operator=(const EnemyComponent& rhs);

		void UpdateAI(const GameTime& gameTime);
		void HandleManualMovement(const GameTime& gameTime);

		static const float LightModulationRate;
		static const float LightMovementRate;

		Effect* mEffect;
		ID3D11ShaderResourceView* mTextureShaderResourceView;
		ID3D11Buffer* mVertexBuffer;
		ID3D11Buffer* mIndexBuffer;
		UINT mIndexCount;

		Keyboard* mKeyboard;
		XMCOLOR mAmbientColor;
		PointLight* mPointLight;
		XMCOLOR mSpecularColor;
		float mSpecularPower;

		ProxyModel* mProxyModel;

		RenderStateHelper* mRenderStateHelper;
		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;
		XMFLOAT2 mTextPosition;

		XMFLOAT3 mAItarget;
		bool mAiTargetReached;
		bool mAIxReached;
		bool mAIzReached;
		float mAIRotation;
		float mDeltaX;
		float mDeltaY;

		PlayerComponent* mPlayer;

		SoundManager* mSoundmanager;
		StateManager* mStatemanager;
		
	};
}
