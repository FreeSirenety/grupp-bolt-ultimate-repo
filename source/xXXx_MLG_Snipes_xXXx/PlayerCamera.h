#pragma once

#include "Camera.h"

using namespace Library;

namespace Rendering
{
	class PlayerComponent;

	class PlayerCamera : public Camera
	{
		RTTI_DECLARATIONS(PlayerCamera, Camera)

	public:
		PlayerCamera(Game& game, PlayerComponent* playerComponent);
		PlayerCamera(Game& game, PlayerComponent* playerComponent, float fieldOfView, float aspectRatio, float nearPlaneDistance, float farPlaneDistance);

		virtual ~PlayerCamera();

		void SetPlayerComponent(PlayerComponent* playerComponent);
		PlayerComponent* GetPlayerComponent();

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;

	protected:
		PlayerComponent *mPlayer;

	private:
		PlayerCamera(const PlayerCamera& rhs);
		PlayerCamera& operator=(const PlayerCamera& rhs);
	};
}

