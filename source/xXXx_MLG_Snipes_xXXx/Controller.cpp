#include "Controller.h"
#include <math.h>

using namespace Library;

namespace Rendering
{

	RTTI_DEFINITIONS(Controller)

	Controller::Controller(Game& game)
		: GameComponent(game)
	{
		mControllerNum = 0;
	}

	void Controller::Initialize()
	{

	}

	void Controller::Update(const GameTime& /*gameTime*/)
	{

	}

	XINPUT_STATE Controller::GetState()
	{
		ZeroMemory(&mControllerState, sizeof(XINPUT_STATE));

		XInputGetState(mControllerNum, &mControllerState);

		return mControllerState;
	}

	bool Controller::IsConnected()
	{
		ZeroMemory(&mControllerState, sizeof(XINPUT_STATE));

		DWORD result = XInputGetState(mControllerNum, &mControllerState);

		if (result == ERROR_SUCCESS)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void Controller::Vibrate(int /*leftval*/, int /*rightval*/)
	{
	/*	XINPUT_VIBRATION vibration;

		ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));

		vibration.wLeftMotorSpeed = leftval;
		vibration.wRightMotorSpeed = rightval;

		XInputSetState(mControllerNum, &vibration);*/
		return;
	}

	float Controller::GetRightAngle()
	{
		float x, y;

		GetState();

		x = (float)mControllerState.Gamepad.sThumbRX;
		y = (float)mControllerState.Gamepad.sThumbRY;

		return atan2f(y, x) + XM_PI / 2;
	}

	XMVECTOR Controller::GetLeftVector()
	{
		float x, y;

		GetState();

		x = (float)mControllerState.Gamepad.sThumbLX;
		y = (float)-mControllerState.Gamepad.sThumbLY;

		XMVECTOR vector = XMVECTOR();

		vector = XMVectorSetX(vector, x);
		vector = XMVectorSetZ(vector, y);

		return vector;
	}
}