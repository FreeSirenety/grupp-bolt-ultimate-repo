#include "MLGGame.h"
#include "GameException.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "FpsComponent.h"
#include "ColorHelper.h"
#include "PlayerCamera.h"
#include "RenderStateHelper.h"
#include "RasterizerStates.h"
#include "SamplerStates.h"
#include "Grid.h"
#include "Skybox.h"
#include "SoundManager.h"
#include "Sound_task.h"
#include "libTask/tasks/task_execute_every.h"
#include "DrawManager.h"
#include "StateManager.h"
#include "GameObjectManager.h"
#include "PlayerComponent.h"
#include "Cursor.h"
#include "PlayerMaterial.h"
#include "EnemyComponent.h"
#include "EnemyMaterial.h"
#include "BackgroundObjects.h"
#include "Controller.h"
#include "GameObjectManager.h"
#include "CollisionManager.h"
#include "MouseCursor.h"

namespace Rendering
{
	const XMVECTORF32 MLGGame::BackgroundColor = ColorHelper::CornflowerBlue;

	MLGGame::MLGGame(HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand)
		: Game(instance, windowClass, windowTitle, showCommand),
		mFpsComponent(nullptr),
		mDirectInput(nullptr), mKeyboard(nullptr), mMouse(nullptr), mRenderStateHelper(nullptr), mGrid(nullptr), TriggerExit(false)
	{
		mDepthStencilBufferEnabled = true;
		mMultiSamplingEnabled = true;

		this->task_manager = new Task_manager(8); //Initialize with 8 threads. More than enough, but most will be sleeping all the time
	}

	MLGGame::~MLGGame()
	{
		task_manager->stop();
		delete task_manager;
		delete sound_task;
		delete timer;
	}

	void MLGGame::Initialize()
	{
		if (FAILED(DirectInput8Create(mInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (LPVOID*)&mDirectInput, nullptr)))
		{
			throw GameException("DirectInput8Create() failed");
		}

		this->mSoundManager = new ::SoundManager();
		mComponents[GameObjectComponent::Nondrawable].push_back(this->mSoundManager);
		mServices.AddService(SoundManager::TypeIdClass(), mSoundManager);
		this->sound_task = new Sound_task(mSoundManager);
		this->timer = new Task_execute_every(std::chrono::milliseconds(1), sound_task);
		//t->set_delete_on_complete(true);
		this->task_manager->add_task(timer);

		mSoundManager->PlayMusic("..\\..\\data\\music\\2001Space.wav");

		this->mCollisionManager = new CollisionManager(mComponents, *this);
		mComponents[GameComponent::Nondrawable].push_back(this->mCollisionManager);
		mServices.AddService(CollisionManager::TypeIdClass(), mCollisionManager);

		mKeyboard = new Keyboard(*this, mDirectInput);
		mComponents[GameObjectComponent::Nondrawable].push_back(mKeyboard);
		mServices.AddService(Keyboard::TypeIdClass(), mKeyboard);

		mMouse = new Mouse(*this, mDirectInput);
		mComponents[GameObjectComponent::Nondrawable].push_back(mMouse);
		mServices.AddService(Mouse::TypeIdClass(), mMouse);

		mCamera = new PlayerCamera(*this, mPlayerComponent);
		mComponents[GameObjectComponent::Nondrawable].push_back(mCamera);
		mServices.AddService(Camera::TypeIdClass(), mCamera);

		mFpsComponent = new FpsComponent(*this);
		mFpsComponent->Initialize();

		mMouseCursorComponent = new MouseCursorComponent(*this, *mCamera, *mMouse);
		mMouseCursorComponent->Initialize();

		mGameObjectManager = new GameObjectManager(*this, *mCamera);
		mServices.AddService(GameObjectComponent::TypeIdClass(), mGameObjectManager);

		mPlayerComponent = mGameObjectManager->CreateGameObject<PlayerComponent>(GameComponent::Player);
		mServices.AddService(PlayerComponent::TypeIdClass(), mPlayerComponent);
		mComponents[GameObjectComponent::Player].push_back(mPlayerComponent);

		for (int i = 0; i < 10; i++)
		{
			mEnemyComponent = mGameObjectManager->CreateGameObject<EnemyComponent>(GameComponent::Enemy);
			mComponents[GameObjectComponent::Enemy].push_back(mEnemyComponent);
		}
		

		mBackgroundObjects = new BackgroundObjects(*this, *mCamera);
		mComponents[GameObjectComponent::EObjectType::Standard].push_back(mBackgroundObjects);

		this->mStateManager = new StateManager();
		mComponents[GameComponent::Nondrawable].push_back(this->mStateManager);
		mServices.AddService(StateManager::TypeIdClass(), mStateManager);
		
		

		mCamera->SetPlayerComponent(mPlayerComponent);

		mGrid = new Grid(*this, *mCamera);
		mComponents[GameObjectComponent::Grid].push_back(mGrid);

		//Skybox
		mSkybox = new Skybox(*this, *mCamera, L"Content\\Textures\\texture2.dds", 500.0f);
		mComponents[GameObjectComponent::Skybox].push_back(mSkybox);

		RasterizerStates::Initialize(mDirect3DDevice);
		SamplerStates::BorderColor = ColorHelper::Black;
		SamplerStates::Initialize(mDirect3DDevice);

		mRenderStateHelper = new RenderStateHelper(*this);

		Game::Initialize();

		mCamera->SetPosition(0.0f, 0.0f, 25.0f);

		mController = new Controller(*this);
		mServices.AddService(Controller::TypeIdClass(), mController);
	}

	void MLGGame::Shutdown()
	{
		DeleteObject(mRenderStateHelper);
		DeleteObject(mFpsComponent);
		DeleteObject(mMouseCursorComponent);
		DeleteObject(mGameObjectManager);
		DeleteObject(mController);
		for (auto ComponentPair : mComponents)
		{
			for (auto Component : ComponentPair.second)
			{
				DeleteObject(Component);
			}
		}
		

		ReleaseObject(mDirectInput);
		RasterizerStates::Release();
		SamplerStates::Release();

		Game::Shutdown();
	}

	void MLGGame::Update(const GameTime &gameTime)
	{
		mFpsComponent->Update(gameTime);
		

		if (mKeyboard->WasKeyPressedThisFrame(DIK_ESCAPE))
		{
			Exit();
		}

		if (mKeyboard->WasKeyPressedThisFrame(DIK_Q))
		{
			mComponents[GameObjectComponent::Enemy][0]->TriggerDestroy();
		}

		Game::Update(gameTime);

		for (auto componentPair : mComponents)
		{
			for (auto component : componentPair.second)
			{
				if (component != nullptr && component != NULL && component->Enabled() && component->Is(DrawableGameComponent::TypeIdClass()))
				{
					for (auto componentPair2 : mComponents)
					{
						for (auto component2 : componentPair2.second)
						{
							if (component2->Enabled() && component->Is(DrawableGameComponent::TypeIdClass()) && component != component2)
							{
								mCollisionManager->isColliding(component, component2);
							}
						}
					}
				}
			}
		}

		if (mComponents[GameComponent::Enemy].size() == 0)
		{
			Exit();
		}

		mMouseCursorComponent->Update(gameTime);

		if (TriggerExit)
			Exit();
	}

	void MLGGame::Draw(const GameTime &gameTime)
	{
		mDirect3DDeviceContext->ClearRenderTargetView(mRenderTargetView, reinterpret_cast<const float*>(&BackgroundColor));
		mDirect3DDeviceContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

		Game::Draw(gameTime);

		mGrid->Draw(gameTime);

		mRenderStateHelper->SaveAll();
		mFpsComponent->Draw(gameTime);
		mMouseCursorComponent->Draw(gameTime);
		mRenderStateHelper->RestoreAll();

		HRESULT hr = mSwapChain->Present(0, 0);
		if (FAILED(hr))
		{
			throw GameException("IDXGISwapChain::Present() failed.", hr);
		}
	}

	GameObjectManager* MLGGame::GetObjectManager()
	{
		return mGameObjectManager;
	}
}