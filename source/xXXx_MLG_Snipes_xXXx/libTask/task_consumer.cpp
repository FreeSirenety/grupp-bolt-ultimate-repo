/*
Copyright (C) 2015  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "task_consumer.h"
#include "task_manager.h"
#include "tasks/task_print.h"

Task_consumer::Task_consumer(Task_manager *manager)
{
	this->manager = manager;
}

void Task_consumer::run()
{
	Task* t = NULL;
	while((t = manager->get_task()) != NULL){
		t->execute(manager);
		t->set_executed(true);

		if(t->delete_on_completion()){
			if(t->depending_completed()){
				delete t;
			}
			else
				manager->add_task_for_deletion(t);
		}
	}
}


void task_consumer_thread(Task_consumer consumer)
{
	//printf("Starting consumer thread.\r\n");
	consumer.run();
	//printf("Consumer thread completed.\r\n");
}
