/*
Copyright (C) 2015  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef TASK_H
#define TASK_H
#include <list>
class Task_consumer;
class Task_manager;
/**
 * @brief Task class to be implemented by tasks
 * This is the interface to be implemented by new tasks.
 * The function execute() must be overridden by all tasks.
 */
class Task
{
	friend class Task_consumer;
public:
	/**
	 * @brief Task
	 * @param name to be returned by the get_name() function
	 */
	Task(const char* name);
	virtual ~Task();
	/**
	 * @brief completed
	 * @return True if task is completed
	 */
	bool completed();
	/**
	 * @brief is_executable
	 * @return True if all dependencies are met
	 */
	bool is_executable();
	/**
	 * @brief add_dependency
	 * @param task this task will depend on
	 * Adds a dependency of another task before this task can be executed
	 */
	void add_dependency(Task* task);
	/**
	 * @brief execute
	 * @param manager
	 * Must be implemented in all Tasks
	 */
	virtual void execute(Task_manager* manager) = 0;
	virtual void* result();
	/**
	 * @brief get_name
	 * @return Name of the task
	 */
	char* get_name();
	/**
	 * @brief delete_on_completion
	 * @return True if task will be deleted when executed
	 */
	bool delete_on_completion();
	/**
	 * @brief set_delete_on_complete
	 * @param value True to delete task on completion, False to not delete task on completion
	 */
	void set_delete_on_complete(bool value);
	/**
	 * @brief depending_completed
	 * @return True if all tasks depending on this task are completed
	 */
	bool depending_completed();
private:
	char* name;
	bool delete_on_complete;
	void add_depended_on_by(Task* task);
	virtual void set_executed(bool executed);
protected:
	bool executed;
	std::list<Task*> dependencies;
	std::list<Task*> depended_on_by;
};

#endif // TASK_H
