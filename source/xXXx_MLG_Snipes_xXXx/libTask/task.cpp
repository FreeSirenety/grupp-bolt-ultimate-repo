/*
Copyright (C) 2015  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <string.h>
#include "task.h"


Task::Task(const char *name)
{
	
	this->name = new char[strlen(name)+1];
	int len = strlen(name) + 1;
	//strncpy(this->name, name, strlen(name));

	strncpy_s(this->name, len, name, len);
	this->name[strlen(name)] = 0;
	this->delete_on_complete = false;
	executed = false;
}

Task::~Task(){
	delete[] this->name;
}

bool Task::completed()
{
	return executed;
}

bool Task::is_executable()
{
	for(Task* t : dependencies)
		if(!t->completed())
			return false;
	return true;
}

void Task::add_dependency(Task *task)
{
	this->dependencies.push_back(task);
	task->add_depended_on_by(this);
}

void* Task::result()
{
	return NULL;
}

void Task::add_depended_on_by(Task* task)
{
	this->depended_on_by.push_back(task);
}

void Task::set_executed(bool executed)
{
	this->executed = executed;
}

char* Task::get_name()
{
	return name;
}
bool Task::delete_on_completion()
{
	return delete_on_complete;
}

void Task::set_delete_on_complete(bool value)
{
	delete_on_complete = value;
}

bool Task::depending_completed()
{
	for(Task* t : depended_on_by)
		if(!t->completed())
			return false;
	return true;
}

