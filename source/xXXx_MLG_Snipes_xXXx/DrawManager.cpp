#include "DrawManager.h"
#include "Effect.h"
#include "Material.h"
#include "DrawableGameComponent.h"
#include "GameObjectComponent.h"
#include "PlayerComponent.h"
#include "EnemyComponent.h"
#include "Skybox.h"
#include "Grid.h"
#include "BulletComponent.h"

using namespace Library;

namespace Rendering
{
	RTTI_DEFINITIONS(DrawManager)

	DrawManager::DrawManager(Game& game)
	: GameComponent(game) 
	{

	}

	DrawManager::~DrawManager()
	{

	}

	void DrawManager::Initialize()
	{
		
	}

	void DrawManager::DrawObject(DrawableGameComponent* drawableComponent, const GameTime& gameTime)
	{
		
		if (drawableComponent->Is(PlayerComponent::TypeIdClass()))
		{
			if (CheckCurrentObjectType(EObjectType::Player))
			{
				drawableComponent->Draw(gameTime, true);
			}
			else
			{
				drawableComponent->Draw(gameTime);
			}
		}
		else if (drawableComponent->Is(EnemyComponent::TypeIdClass()))
		{
			if (CheckCurrentObjectType(EObjectType::Enemy))
			{
				drawableComponent->Draw(gameTime, true);
			}
			else
			{
				drawableComponent->Draw(gameTime);
			}
		}
		else if (drawableComponent->Is(Skybox::TypeIdClass()))
		{
			if (CheckCurrentObjectType(EObjectType::Skybox))
			{
				drawableComponent->Draw(gameTime, true);
			}
			else
			{
				drawableComponent->Draw(gameTime);
			}
		}
		else if (drawableComponent->Is(Grid::TypeIdClass()))
		{
			if (CheckCurrentObjectType(EObjectType::Grid))
			{
				drawableComponent->Draw(gameTime, true);
			}
			else
			{
				drawableComponent->Draw(gameTime);
			}
		}
		else if (drawableComponent->Is(BulletComponent::TypeIdClass()))
		{
			if (CheckCurrentObjectType(EObjectType::Bullet))
			{
				drawableComponent->Draw(gameTime, true);
			}
			else
			{
				drawableComponent->Draw(gameTime);
			}
		}
		else if (drawableComponent->Is(GameObjectComponent::TypeIdClass()))
		{
			if (CheckCurrentObjectType(EObjectType::Standard))
			{
				drawableComponent->Draw(gameTime, true);
			}
			else
			{
				drawableComponent->Draw(gameTime);
			}
		}
	}

	bool DrawManager::CheckCurrentObjectType(EObjectType currentObjectType)
	{
		if (mCurrentObjectType == currentObjectType)
		{
			return true;
		}
		else
		{
			mCurrentObjectType = currentObjectType;
			return false;
		}
	}

	void DrawManager::SetObjectProperties(EObjectType objectType, Effect* effect, Material* material)
	{
		auto pair = std::pair<EObjectType, PropertyStruct>(objectType, PropertyStruct{ effect, material });

		mObjectPropreties.insert(pair);
	}

	void DrawManager::SetCurrentObjectType(EObjectType objectType)
	{
		mCurrentObjectType = objectType;
	}
}