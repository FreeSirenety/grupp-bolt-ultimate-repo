//GameState.h

#pragma once
#include "State.h"
#include <string>




class GameState : public State {
public:
	GameState();

	bool Enter();
	void Exit();
	void Pause();
	void Resume();
	bool Update(float deltatime);
	void Draw();
	std::string Next();
	bool IsType(const std::string &type);
	bool isPaused();
	bool isBase();

private:

	
private:

	// STATE VARIABLES
	
	bool m_paused;
	bool m_base;



};