// StateManager.h
#pragma once
#include "Common.h"
#include "GameComponent.h"
enum States
{
	STARTSTATE,
	GAMESTATE,
	OPTIONSTATE
};

class StateManager : public Library::GameComponent
{
	RTTI_DECLARATIONS(StateManager, Library::GameComponent)
public:
	StateManager();
	~StateManager();

	virtual void Initialize() override;
	virtual void Update(const Library::GameTime& gameTime) override;

	void QueuedStateChange();
	void SetQueue(States state);
	States GetState();

private:
	bool mQueue;
	States mCurrentState;
	States mQueuedState;
};

