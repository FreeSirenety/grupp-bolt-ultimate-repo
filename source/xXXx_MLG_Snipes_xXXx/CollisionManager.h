//CollisionManager.h

#include "GameObjectComponent.h"
#include "PlayerComponent.h"
#include "DrawableGameComponent.h"
#include "SoundManager.h"
#include "Game.h"
#include "GameComponent.h"


//namespace Rendering
//{

	class CollisionManager : public GameComponent {

		RTTI_DECLARATIONS(CollisionManager, GameComponent)

	public:
		CollisionManager(std::map<GameComponent::EObjectType, std::vector<GameComponent*>> mComponents, Game& game);
		~CollisionManager();

		int isColliding(GameComponent* self, GameComponent* other);

		virtual void Initialize() override;
		virtual void Update(const Library::GameTime& gameTime) override;

	private:

	
		std::map<GameComponent::EObjectType, std::vector<GameComponent*>> mCompColl;

		SoundManager* mSoundmanager;
	};

//}