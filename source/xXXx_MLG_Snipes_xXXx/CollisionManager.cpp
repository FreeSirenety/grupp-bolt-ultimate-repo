//CollisionManager.cpp
#include "CollisionManager.h"
#include "GameObjectComponent.h"
#include "PlayerComponent.h"
#include "BulletComponent.h"
#include "EnemyComponent.h"
#include "DrawableGameComponent.h"
#include "Game.h"
#include "SoundManager.h"
#include "Windows.h"
#include "control.h"
#include "GameTime.h"
#include "GameComponent.h"




using namespace Rendering;

//namespace Rendering
//{

	RTTI_DEFINITIONS(CollisionManager)


		CollisionManager::CollisionManager(std::map<GameComponent::EObjectType, std::vector<GameComponent*>> mComponents, Game& game) : GameComponent(game)
	{
			mCompColl = mComponents;
			
	}
	CollisionManager::~CollisionManager()
	{

	}

	int CollisionManager::isColliding(GameComponent* self, GameComponent* other)
	{
		//Move(xMovement * (float)gameTime.ElapsedGameTime() / 500, 0.f, zMovement * (float)gameTime.ElapsedGameTime() / 500);
		
	
		GameObjectComponent *mSelf = static_cast<GameObjectComponent*>(self);
		GameObjectComponent *mOther = static_cast<GameObjectComponent*>(other);
		

		XMVECTOR XMVself = mSelf->GetPosition();
		XMVECTOR XMVother = mOther->GetPosition();


			float px, pz, x, z;
			px = XMVectorGetX(XMVself);
			pz = XMVectorGetZ(XMVself);


			x = XMVectorGetX(XMVother);
			z = XMVectorGetZ(XMVother);

			if (sqrtf((x - px)*(x - px) + (z - pz)*(z - pz)) < 6.f)
			{

				if (mSelf->Is(PlayerComponent::TypeIdClass()))
				{

				}
				else if (mSelf->Is(BulletComponent::TypeIdClass()))
				{
					if (mOther->Is(EnemyComponent::TypeIdClass()))
					{
						EnemyComponent* enemy = mOther->As<EnemyComponent>();
						BulletComponent* bullet = mSelf->As<BulletComponent>();
						mSoundmanager->PlayEffect("..\\..\\data\\sound\\Coin.wav");
						enemy->TriggerDestroy();
						bullet->TriggerDestroy();
					}
				}
			}
		

		return 0;
	}


	void CollisionManager::Initialize()
	{
		mSoundmanager = (SoundManager*)mGame->Services().GetService(SoundManager::TypeIdClass());
		if (mGame == nullptr)
		{
			return;
		}
	}
	void CollisionManager::Update(const Library::GameTime& /*gameTime*/)
	{

		

		
	}

//}
