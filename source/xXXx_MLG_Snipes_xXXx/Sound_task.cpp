#include "Windows.h"
#include "Sound_task.h"
#include "libTask\task_manager.h"


Sound_task::Sound_task(SoundManager* sound) : Task("Sound task")
{
	this->m_sound_man = sound;
}


Sound_task::~Sound_task()
{
}

void Sound_task::execute(Task_manager* manager){
	manager = nullptr;
	/*
	Do sound stuff here
	*/
	m_sound_man->Update();
	Sleep(1);
}

void Sound_task::set_executed(bool executed){
	//Do nothing
	(void)executed;
}
