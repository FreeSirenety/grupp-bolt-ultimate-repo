// SoundManager.cpp
#include "SoundManager.h"

RTTI_DEFINITIONS(SoundManager)
SoundManager::SoundManager()
{
	m_numDrives = 0;
	global_volume = 1.0;
	music_volume = 0.5f;
	effect_volume = 0.5f;
	InitializeSoundManager();
}

SoundManager::~SoundManager()
{
	for (Sounds* s : m_SoundBank) // Winrar!!
	{
		s->Sound->release();
		delete s;
	}
	m_SoundBank.clear();


	for (Effects* s : m_EffectBank)
	{
		s->Sound->release();
		delete s;
	}
	m_EffectBank.clear();
	
}

void SoundManager::Initialize()
{
	InitializeSoundManager();
}
void SoundManager::Update(const Library::GameTime& gameTime)
{
	(void)gameTime;
}


void SoundManager::InitializeSoundManager()
{
	m_result = FMOD::System_Create(&m_system);
	FMODErrorcheck(m_result);

	m_result = m_system->getNumDrivers(&m_numDrives);
	FMODErrorcheck(m_result);

	if (m_numDrives == 0)
	{
		m_result = m_system->setOutput(FMOD_OUTPUTTYPE_NOSOUND);
		FMODErrorcheck(m_result);
	}
	else
	{
		m_result = m_system->setSoftwareFormat(0, FMOD_SPEAKERMODE_DEFAULT, 0);
		FMODErrorcheck(m_result);
	}

	m_result = m_system->init(32, FMOD_INIT_NORMAL, 0);
	if (m_result == FMOD_ERR_OUTPUT_CREATEBUFFER)
	{
		m_result = m_system->setSoftwareFormat(0, FMOD_SPEAKERMODE_STEREO, 0);
		FMODErrorcheck(m_result);

		m_result = m_system->init(32, FMOD_INIT_NORMAL, 0);
	}
	FMODErrorcheck(m_result);

	m_system->update();
	
}

void SoundManager::FMODErrorcheck(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
	//	std::cout << "FMOD Error! (" << result << ")" << FMOD_ErrorString(result) << std::endl;
		//exit(-1);
	}
}
void SoundManager::LoadEffect(std::string path)
{
	Effects* info = new Effects;
	info->path = path;

	m_result = m_system->createSound(info->path.c_str(), FMOD_DEFAULT, 0, &info->Sound);
	FMODErrorcheck(m_result);
	//info.Channel = info.Sound->

	m_EffectBank.push_back(info);
}

void SoundManager::LoadMusic(std::string path)
{
	Sounds* info = new Sounds;
	info->path = path;
	m_system->createStream(info->path.c_str(), FMOD_DEFAULT, 0, &info->Sound);
	
	m_SoundBank.push_back(info);
}


void SoundManager::PlayMusic(std::string path)
{
	for (Sounds* s : m_SoundBank) // Winrar!!
	{
		if (s->path == path)
		{
			m_system->playSound(s->Sound, 0, false, &s->Channel);
			return;
		}
	}

	LoadMusic(path);

	PlayMusic(path);

}

void SoundManager::PlayEffect(std::string path)
{
	for (Effects* s : m_EffectBank)
	{
		if (s->path == path)
		{
			m_system->playSound(s->Sound, 0, false, &s->Channel);
			return;
		}
	}
	LoadEffect(path);
	PlayEffect(path);
}

void SoundManager::Update()
{
	m_system->update();
}

void SoundManager::SetVolume(float vol)
{
	this->global_volume = vol;
	SetMusicVolume(music_volume);
	SetEffectVolume(effect_volume);
}

void SoundManager::SetMusicVolume(float vol)
{
	music_volume = vol;
	for (Sounds* s : m_SoundBank)
	{
		s->Channel->setVolume(vol*global_volume);
	}
}

void SoundManager::SetEffectVolume(float vol)
{
	effect_volume = vol;
	for (Effects* s : m_EffectBank)
	{
		s->Channel->setVolume(vol*global_volume);
	}

}
