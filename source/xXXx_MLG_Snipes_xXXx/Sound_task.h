#pragma once
#include "libTask\task.h"
#include "SoundManager.h"
class Sound_task :
	public Task
{
public:
	Sound_task(SoundManager* sound);
	~Sound_task();
	void execute(Task_manager* manager);
	void set_executed(bool executed);
private:
	SoundManager* m_sound_man;
};

