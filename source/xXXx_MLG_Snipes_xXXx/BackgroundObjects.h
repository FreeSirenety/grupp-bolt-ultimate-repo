// BackgroundObjects.h



#pragma once

#include "DrawableGameComponent.h"

using namespace Library;

namespace Library
{
	class Effect;
	class PointLight;
	class ProxyModel;
	class RenderStateHelper;
	class Keyboard;
	class Mouse;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class PlayerMaterial;
	class PlayerCamera;

	class BackgroundObjects : public DrawableGameComponent
	{
		RTTI_DECLARATIONS(BackgroundObjects, DrawableGameComponent)

	public:
		BackgroundObjects(Game& game, Camera& camera);
		~BackgroundObjects();

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime, bool KeepSettings = false) override;

		XMFLOAT4X4 GetWorldMatrix();

		void SetPosition(XMFLOAT3 position);
		XMFLOAT3 GetPosition();

		void Move(float x, float y, float z);

	private:
		BackgroundObjects();
		BackgroundObjects(const BackgroundObjects& rhs);
		BackgroundObjects& operator=(const BackgroundObjects& rhs);

		void UpdateAmbientLight(const GameTime& gameTime);
		void UpdatePointLight(const GameTime& gameTime);
		void UpdateSpecularLight(const GameTime& gameTime);

		static const float LightModulationRate;
		static const float LightMovementRate;

		Effect* mEffect;
		PlayerMaterial* mMaterial;
		ID3D11ShaderResourceView* mTextureShaderResourceView;
		ID3D11Buffer* mVertexBuffer;
		ID3D11Buffer* mIndexBuffer;
		UINT mIndexCount;

		Keyboard* mKeyboard;
		XMCOLOR mAmbientColor;
		PointLight* mPointLight;
		XMCOLOR mSpecularColor;
		float mSpecularPower;
		XMFLOAT4X4 mWorldMatrix;

		ProxyModel* mProxyModel;

		RenderStateHelper* mRenderStateHelper;
		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;
		XMFLOAT2 mTextPosition;

		XMFLOAT3 mPosition;
	};
}
