#pragma once

#include "Common.h"
#include "Game.h"
#include "libTask\task_manager.h"

using namespace Library;

namespace Library
{
	class Keyboard;
	class Mouse;
	class FpsComponent;
	class RenderStateHelper;
	class Grid;
	class Skybox;
	//class SoundManager;
}
class SoundManager;
class StateManager;
class CollisionManager;
namespace Rendering
{

	class PlayerComponent;
	class EnemyComponent;
	class PlayerCamera;
	class BackgroundObjects;
	class Controller;
	class GameObjectManager;

	class MouseCursorComponent;

	class MLGGame : public Game
	{
	public:
		MLGGame(HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand);
		~MLGGame();

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime) override;

		GameObjectManager* GetObjectManager();

		bool TriggerExit;

	protected:
		virtual void Shutdown() override;

	private:
		static const XMVECTORF32 BackgroundColor;

		LPDIRECTINPUT8 mDirectInput;
		Keyboard* mKeyboard;
		Mouse* mMouse;
		PlayerCamera* mCamera;
		FpsComponent* mFpsComponent;
		RenderStateHelper* mRenderStateHelper;
		Grid* mGrid;
		PlayerComponent* mPlayerComponent;
		EnemyComponent* mEnemyComponent;
		BackgroundObjects* mBackgroundObjects;
		Skybox* mSkybox;
		Task_manager* task_manager;
		SoundManager* mSoundManager;
		StateManager* mStateManager;
		CollisionManager* mCollisionManager;
		Controller* mController;
		MouseCursorComponent* mMouseCursorComponent;

		GameObjectManager *mGameObjectManager;
		Task* sound_task, *timer;
	};
}