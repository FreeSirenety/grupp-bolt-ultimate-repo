#pragma once

#include "DrawableGameComponent.h"
#include "SoundManager.h"
#include "PlayerComponent.h"

using namespace Library;

namespace Library
{
	class Effect;
	class PointLight;
	class ProxyModel;
	class RenderStateHelper;
	class Keyboard;
	class Mouse;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class EnemyMaterial;
	class PlayerCamera;

	class Cursor : public GameObjectComponent
	{
		RTTI_DECLARATIONS(Cursor, GameObjectComponent)

	public:
		Cursor(Game& game, Camera& camera);
		~Cursor();

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime, bool KeepSettings = false) override;

	

		virtual Material* CreateMaterial() override;
	private:
		Cursor();
		Cursor(const Cursor& rhs);
		Cursor& operator=(const Cursor& rhs);
		void HandleManualMovement(const GameTime& gameTime);

		static const float LightModulationRate;
		static const float LightMovementRate;

		Effect* mEffect;
		ID3D11ShaderResourceView* mTextureShaderResourceView;
		ID3D11Buffer* mVertexBuffer;
		ID3D11Buffer* mIndexBuffer;
		UINT mIndexCount;

		Keyboard* mKeyboard;
		Mouse* mMouse;
		XMCOLOR mAmbientColor;
		PointLight* mPointLight;
		XMCOLOR mSpecularColor;
		float mSpecularPower;

		ProxyModel* mProxyModel;

		RenderStateHelper* mRenderStateHelper;
		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;
		XMFLOAT2 mTextPosition;

	
		float mDeltaX;
		float mDeltaY;

		PlayerComponent* mPlayer;

		SoundManager* mSoundmanager;
		StateManager* mStatemanager;

	};
}
