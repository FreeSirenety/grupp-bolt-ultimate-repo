// main.cpp

#include "StateManager.h"
#include "StartState.h"
#include "GameState.h"
#include "OptionState.h"
#include <iostream>

int main(int argc, char* argv[])
{
	StateManager *m_StateManager = new StateManager;

	m_StateManager->Attach(new StartState(m_StateManager));
	m_StateManager->Attach(new GameState(m_StateManager));
	m_StateManager->Attach(new OptionState(m_StateManager));
	m_StateManager->SetState("StartState");
	bool run = true;

	while (run)
	{
		m_StateManager->Update(0.01f);
	}

	delete m_StateManager;
	m_StateManager = nullptr;

	return 0;
}