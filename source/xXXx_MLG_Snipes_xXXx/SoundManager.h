// SoundManager.h
#pragma once
#include "Common.h"
#include "GameComponent.h"
#include "fmod.hpp"
#include "fmod_errors.h"
#pragma comment(lib, "fmod_vc.lib")
#pragma comment(lib, "fmodL_vc.lib")
#include <iostream>
#include <Windows.h>
#include <string>
#include <vector>
#include "GameTime.h"

using namespace Library;

struct Sounds
{
	FMOD::Channel* Channel;
	FMOD::Sound* Sound;
	std::string path;
	
};
struct Effects
{
	FMOD::Channel* Channel;
	FMOD::Sound* Sound;
	std::string path;
	
};
class SoundManager : public Library::GameComponent
{
	RTTI_DECLARATIONS(SoundManager, Library::GameComponent)
public:
	SoundManager();
	~SoundManager();
	void InitializeSoundManager();
	void PlayMusic(std::string path);
	void PlayEffect(std::string path);

	void SetVolume(float vol);
	void SetMusicVolume(float vol);
	void SetEffectVolume(float vol);
	
	void Update();
	//
	virtual void Initialize() override;
	virtual void Update(const Library::GameTime& gameTime) override;
	//
private:
	void FMODErrorcheck(FMOD_RESULT result);
	void LoadEffect(std::string path);
	void LoadMusic(std::string path);

private:
	int m_numDrives;
	FMOD::System* m_system;
	FMOD_RESULT m_result = FMOD_OK;
	std::vector<Sounds*> m_SoundBank;
	std::vector<Effects*> m_EffectBank;
	float global_volume;
	float music_volume;
	float effect_volume;
};

