#include "PlayerComponent.h"
#include "PlayerMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <DDSTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>
#include "SoundManager.h"
#include "Mouse.h"
#include "BulletComponent.h"
#include "GameObjectManager.h"
#include "MLGGame.h"
#include "Controller.h"
#include <math.h>

namespace Rendering
{
	RTTI_DEFINITIONS(PlayerComponent)

		const float PlayerComponent::LightModulationRate = UCHAR_MAX;
	const float PlayerComponent::LightMovementRate = 10.0f;

	PlayerComponent::PlayerComponent(Game& game, Camera& camera)
		: GameObjectComponent(game, camera), mEffect(nullptr), mTextureShaderResourceView(nullptr),
		mVertexBuffer(nullptr), mIndexBuffer(nullptr), mIndexCount(0),
		mKeyboard(nullptr), mAmbientColor(1, 1, 1, 0), mPointLight(nullptr),
		mSpecularColor(1.0f, 1.0f, 1.0f, 1.0f), mSpecularPower(25.0f), mProxyModel(nullptr),
		mRenderStateHelper(nullptr), mSpriteBatch(nullptr), mSpriteFont(nullptr), mTextPosition(0.0f, 40.0f)
	{
		mRadius = 3.f;
		mTimeSinceLastShot = -5;
	}

	PlayerComponent::~PlayerComponent()
	{
		DeleteObject(mSpriteFont);
		DeleteObject(mSpriteBatch);
		DeleteObject(mRenderStateHelper);
		DeleteObject(mProxyModel);
		DeleteObject(mPointLight);
		ReleaseObject(mTextureShaderResourceView);
		//DeleteObject(mMaterial);
		//DeleteObject(mEffect);
		ReleaseObject(mVertexBuffer);
		ReleaseObject(mIndexBuffer);
	}

	void PlayerComponent::Initialize()
	{
		

		XMStoreFloat4x4(&mWorldMatrix, XMMatrixScaling(0.03f, 0.03f, 0.03f));

		mPointLight = new PointLight(*mGame);
		mPointLight->SetRadius(500.0f);
		mPointLight->SetPosition(5.0f, 0.0f, 10.0f);

		mKeyboard = (Keyboard*)mGame->Services().GetService(Keyboard::TypeIdClass());
		assert(mKeyboard != nullptr);

		mProxyModel = new ProxyModel(*mGame, *mCamera, "Content\\Models\\PointLightProxy.obj", 0.5f);
		mProxyModel->Initialize();

		mRenderStateHelper = new RenderStateHelper(*mGame);

		mSpriteBatch = new SpriteBatch(mGame->Direct3DDeviceContext());
		mSpriteFont = new SpriteFont(mGame->Direct3DDevice(), L"Content\\Fonts\\Arial_14_Regular.spritefont");

		SetScale(0.01f, 0.01f, 0.01f);

		mSoundmanager = (SoundManager*)mGame->Services().GetService(SoundManager::TypeIdClass());
		mMouse = (Mouse*)mGame->Services().GetService(Mouse::TypeIdClass());
		mStatemanager = (StateManager*)mGame->Services().GetService(StateManager::TypeIdClass());
	}

	void PlayerComponent::Update(const GameTime& gameTime)
	{
		GameObjectComponent::Update(gameTime);

		UpdateAmbientLight(gameTime);
		UpdatePointLight(gameTime);
		UpdateSpecularLight(gameTime);

		mProxyModel->Update(gameTime);
		//Rotate(10 * (float)gameTime.ElapsedGameTime());

		Controller* controller = static_cast<Controller*>(mGame->Services().GetService(Controller::TypeIdClass()));

		SetRotation(controller->GetRightAngle());

	/*	static bool a = false;
		if (!a){
			mSoundmanager->PlayMusic("D:\\AAA 3D PROG\\source\\audio.flac");
			a = true;
		}*/
	/*	if (mStatemanager->GetState() == GAMESTATE)
		{
			static bool a = false;
			if (!a){
				mSoundmanager->PlayMusic("D:\\AAA 3D PROG\\source\\audio.flac");
				a = true;
			}
		}*/

		XMVECTOR controllerMovement = controller->GetLeftVector();

		float xMovement = XMVectorGetX(controllerMovement);
		float zMovement = XMVectorGetZ(controllerMovement);

		Move(xMovement * (float)gameTime.ElapsedGameTime() / 500, 0.f, zMovement * (float)gameTime.ElapsedGameTime() / 500);

		if (mKeyboard->IsKeyDown(DIK_W))
		{
			Move(0, 0, -10 * (float)gameTime.ElapsedGameTime());
			//mStatemanager->SetQueue(GAMESTATE);

		}

		if (mKeyboard->IsKeyDown(DIK_S))
		{
			mStatemanager->QueuedStateChange();
			Move(0, 0, 10 * (float)gameTime.ElapsedGameTime());
		}

		if (mKeyboard->IsKeyDown(DIK_A))
		{
			Move(-10 * (float)gameTime.ElapsedGameTime(), 0, 0);
		}

		if (mKeyboard->IsKeyDown(DIK_D))
		{
			Move(10 * (float)gameTime.ElapsedGameTime(), 0, 0);
		}

		mTimeSinceLastShot += (float)gameTime.ElapsedGameTime();
		
		if (mTimeSinceLastShot > 0.3f)
		{
			mTimeSinceLastShot = 0;
			MLGGame* game = static_cast<MLGGame*>(mGame);

			GameObjectManager* gObjManager = game->GetObjectManager();

			BulletComponent *bullet = gObjManager->CreateGameObject<BulletComponent>(EObjectType::Bullet);

			bullet->Initialize();

			XMVECTOR bulletPosition = XMLoadFloat3(&mPosition);

			XMVECTOR bulletVelocity = XMVECTOR();

			bulletVelocity = XMVectorSetZ(bulletVelocity, cosf(mRotation));
			bulletVelocity = XMVectorSetY(bulletVelocity, 0.f);
			bulletVelocity = XMVectorSetX(bulletVelocity, sinf(mRotation));

			bullet->SetRotation(mRotation);

			bullet->TriggerBullet(bulletPosition, bulletVelocity, 6.f, 6.f);

			mGame->AddComponent(bullet, Bullet);
			mSoundmanager->PlayEffect("..\\..\\Data\\sound\\laser.wav");
		}
	}

	void PlayerComponent::Draw(const GameTime& gameTime, bool KeepSettings)
	{
		GameObjectComponent::Draw(gameTime);

		PlayerMaterial* material = static_cast<PlayerMaterial*>(mMaterial);

		if (KeepSettings)
		{
			Pass* pass = mMaterial->CurrentTechnique()->Passes().at(0);

			XMMATRIX worldMatrix = XMLoadFloat4x4(&mWorldMatrix);
			XMMATRIX wvp = worldMatrix * mCamera->ViewMatrix() * mCamera->ProjectionMatrix();
			XMVECTOR ambientColor = XMLoadColor(&mAmbientColor);
			XMVECTOR specularColor = XMLoadColor(&mSpecularColor);

			

			material->WorldViewProjection() << wvp;
			material->World() << worldMatrix;
			material->SpecularColor() << specularColor;
			material->SpecularPower() << mSpecularPower;
			material->AmbientColor() << ambientColor;

			ID3D11DeviceContext* direct3DDeviceContext = mGame->Direct3DDeviceContext();

			pass->Apply(0, direct3DDeviceContext);

			direct3DDeviceContext->DrawIndexed(mMaterial->mIndexCount, 0, 0);
		}
		else
		{
			ID3D11DeviceContext* direct3DDeviceContext = mGame->Direct3DDeviceContext();
			direct3DDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			Pass* pass = mMaterial->CurrentTechnique()->Passes().at(0);
			ID3D11InputLayout* inputLayout = mMaterial->InputLayouts().at(pass);
			direct3DDeviceContext->IASetInputLayout(inputLayout);

			UINT stride = mMaterial->VertexSize();
			UINT offset = 0;
			direct3DDeviceContext->IASetVertexBuffers(0, 1, &mMaterial->mVertexBuffer, &stride, &offset);
			direct3DDeviceContext->IASetIndexBuffer(mMaterial->mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

			XMMATRIX worldMatrix = XMLoadFloat4x4(&mWorldMatrix);
			XMMATRIX wvp = worldMatrix * mCamera->ViewMatrix() * mCamera->ProjectionMatrix();
			XMVECTOR ambientColor = XMLoadColor(&mAmbientColor);
			XMVECTOR specularColor = XMLoadColor(&mSpecularColor);

			material->WorldViewProjection() << wvp;
			material->World() << worldMatrix;
			material->SpecularColor() << specularColor;
			material->SpecularPower() << mSpecularPower;
			material->AmbientColor() << ambientColor;
			material->LightColor() << mPointLight->ColorVector();
			material->LightPosition() << mPointLight->PositionVector();
			material->LightRadius() << mPointLight->Radius();
			material->ColorTexture() << material->mTextureShaderResourceView;
			material->CameraPosition() << mCamera->PositionVector();

			pass->Apply(0, direct3DDeviceContext);

			direct3DDeviceContext->DrawIndexed(mMaterial->mIndexCount, 0, 0);

			mProxyModel->Draw(gameTime);

			mRenderStateHelper->SaveAll();
			mSpriteBatch->Begin();

			std::wostringstream helpLabel;
			helpLabel << L"Ambient Intensity (+PgUp/-PgDn): " << mMouse->X() << "\n";
			helpLabel << L"Point Light Intensity (+Home/-End): " << mPointLight->Color().a << "\n";
			helpLabel << L"Specular Power (+Insert/-Delete): " << mSpecularPower << "\n";
			helpLabel << L"Move Point Light (8/2, 4/6, 3/9)\n";

			mSpriteFont->DrawString(mSpriteBatch, helpLabel.str().c_str(), mTextPosition);

			mSpriteBatch->End();
			mRenderStateHelper->RestoreAll();
		}
	}

	void PlayerComponent::UpdateSpecularLight(const GameTime& gameTime)
	{
		static float specularIntensity = mSpecularPower;

		if (mKeyboard != nullptr)
		{
			if (mKeyboard->IsKeyDown(DIK_INSERT) && specularIntensity < UCHAR_MAX)
			{
				specularIntensity += LightModulationRate * (float)gameTime.ElapsedGameTime();
				specularIntensity = XMMin<float>(specularIntensity, UCHAR_MAX);

				mSpecularPower = specularIntensity;;
			}

			if (mKeyboard->IsKeyDown(DIK_DELETE) && specularIntensity > 0)
			{
				specularIntensity -= LightModulationRate * (float)gameTime.ElapsedGameTime();
				specularIntensity = XMMax<float>(specularIntensity, 0);

				mSpecularPower = specularIntensity;
			}
		}
	}

	void PlayerComponent::UpdatePointLight(const GameTime& gameTime)
	{
		static float pointLightIntensity = mPointLight->Color().a;
		float elapsedTime = (float)gameTime.ElapsedGameTime();

		// Update point light intensity		
		if (mKeyboard->IsKeyDown(DIK_HOME) && pointLightIntensity < UCHAR_MAX)
		{
			pointLightIntensity += LightModulationRate * elapsedTime;

			XMCOLOR pointLightLightColor = mPointLight->Color();
			pointLightLightColor.a = (UCHAR)XMMin<float>(pointLightIntensity, UCHAR_MAX);
			mPointLight->SetColor(pointLightLightColor);
		}
		if (mKeyboard->IsKeyDown(DIK_END) && pointLightIntensity > 0)
		{
			pointLightIntensity -= LightModulationRate * elapsedTime;

			XMCOLOR pointLightLightColor = mPointLight->Color();
			pointLightLightColor.a = (UCHAR)XMMax<float>(pointLightIntensity, 0.0f);
			mPointLight->SetColor(pointLightLightColor);
		}

		// Move point light
		XMFLOAT3 movementAmount = Vector3Helper::Zero;
		if (mKeyboard != nullptr)
		{
			if (mKeyboard->IsKeyDown(DIK_NUMPAD4))
			{
				movementAmount.x = -1.0f;
			}

			if (mKeyboard->IsKeyDown(DIK_NUMPAD6))
			{
				movementAmount.x = 1.0f;
			}

			if (mKeyboard->IsKeyDown(DIK_NUMPAD9))
			{
				movementAmount.y = 1.0f;
			}

			if (mKeyboard->IsKeyDown(DIK_NUMPAD3))
			{
				movementAmount.y = -1.0f;
			}

			if (mKeyboard->IsKeyDown(DIK_NUMPAD8))
			{
				movementAmount.z = -1.0f;
			}

			if (mKeyboard->IsKeyDown(DIK_NUMPAD2))
			{
				movementAmount.z = 1.0f;
			}
		}

		XMVECTOR movement = XMLoadFloat3(&movementAmount) * LightMovementRate * elapsedTime;
		mPointLight->SetPosition(mPointLight->PositionVector() + movement);
		mProxyModel->SetPosition(mPointLight->Position());
	}

	void PlayerComponent::UpdateAmbientLight(const GameTime& gameTime)
	{
		static float ambientIntensity = mAmbientColor.a;

		if (mKeyboard != nullptr)
		{
			if (mKeyboard->IsKeyDown(DIK_PGUP) && ambientIntensity < UCHAR_MAX)
			{
				ambientIntensity += LightModulationRate * (float)gameTime.ElapsedGameTime();
				mAmbientColor.a = (UCHAR)XMMin<float>(ambientIntensity, UCHAR_MAX);
			}

			if (mKeyboard->IsKeyDown(DIK_PGDN) && ambientIntensity > 0)
			{
				ambientIntensity -= LightModulationRate * (float)gameTime.ElapsedGameTime();
				mAmbientColor.a = (UCHAR)XMMax<float>(ambientIntensity, 0);
			}
		}
	}

	Material* PlayerComponent::CreateMaterial()
	{
		SetCurrentDirectory(Utility::ExecutableDirectory().c_str());

		std::unique_ptr<Model> model(new Model(*mGame, "Content\\Models\\Mech_Theo.obj", true));


		// Initialize the material
		mEffect = new Effect(*mGame);
		mEffect->LoadCompiledEffect(L"Content\\Effects\\PointLight.cso");

		PlayerMaterial* material = new PlayerMaterial();
		material->Initialize(mEffect);

		Mesh* mesh = model->Meshes().at(0);
		material->CreateVertexBuffer(mGame->Direct3DDevice(), *mesh, &material->mVertexBuffer);
		mesh->CreateIndexBuffer(&material->mIndexBuffer);
		material->mIndexCount = mesh->Indices().size();

		std::wstring textureName = L"Content\\Textures\\AssignmentAdvanced_Mech_D.dds";
		HRESULT hr = DirectX::CreateDDSTextureFromFile(mGame->Direct3DDevice(), textureName.c_str(), nullptr, &material->mTextureShaderResourceView);
		if (FAILED(hr))
		{
			throw GameException("CreateDDSTextureFromFile() failed.", hr);
		}

		return material;
	}
}