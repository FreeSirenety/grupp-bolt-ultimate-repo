#pragma once

#include "GameComponent.h"

using namespace Library;

namespace Library
{
	class DrawableGameComponent;
	class Effect;
	class Material;
	class GameTime;
}

namespace Rendering
{
	class DrawManager : public GameComponent
	{

		RTTI_DECLARATIONS(DrawManager, GameComponent)

	public:
		DrawManager(Game& game);
		~DrawManager();

		virtual void Initialize() override;

		void DrawObject(DrawableGameComponent* drawableComponent, const GameTime& gameTime);

		void SetObjectProperties(EObjectType objectType, Effect* effect, Material* material);

		void SetCurrentObjectType(EObjectType objectType);

		bool CheckCurrentObjectType(EObjectType currentObjectType);

	private:
		struct PropertyStruct
		{
			Effect* effect;
			Material* material;
		};

	private:
		std::map<EObjectType, PropertyStruct> mObjectPropreties;

		EObjectType mCurrentObjectType;

	};
}