#pragma once

#include "DrawableGameComponent.h"
#include "SoundManager.h"
#include "GameObjectComponent.h"
#include "StateManager.h"
#pragma comment(lib, "XInput.lib")

using namespace Library;

namespace Library
{
	class Effect;
	class PointLight;
	class ProxyModel;
	class RenderStateHelper;
	class Keyboard;
	class Mouse;
	class Material;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class PlayerMaterial;
	class PlayerCamera;

	class PlayerComponent : public GameObjectComponent
	{
		RTTI_DECLARATIONS(PlayerComponent, GameObjectComponent)

	public:
		PlayerComponent(Game& game, Camera& camera);
		~PlayerComponent();

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime, bool KeepSettings = false) override;

		virtual Material* CreateMaterial() override;

	private:
		PlayerComponent();
		PlayerComponent(const PlayerComponent& rhs);
		PlayerComponent& operator=(const PlayerComponent& rhs);

		void UpdateAmbientLight(const GameTime& gameTime);
		void UpdatePointLight(const GameTime& gameTime);
		void UpdateSpecularLight(const GameTime& gameTime);

		static const float LightModulationRate;
		static const float LightMovementRate;

		float mTimeSinceLastShot;

		Effect* mEffect;
		ID3D11ShaderResourceView* mTextureShaderResourceView;
		ID3D11Buffer* mVertexBuffer;
		ID3D11Buffer* mIndexBuffer;
		UINT mIndexCount;

		Keyboard* mKeyboard;
		XMCOLOR mAmbientColor;
		PointLight* mPointLight;
		XMCOLOR mSpecularColor;
		float mSpecularPower;
		Mouse* mMouse;

		ProxyModel* mProxyModel;

		RenderStateHelper* mRenderStateHelper;
		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;
		XMFLOAT2 mTextPosition;

		SoundManager* mSoundmanager;
		StateManager* mStatemanager;
	};
}
