#include "MouseCursor.h"
#include "Game.h"
#include "Mouse.h"
#include <sstream>
#include <iomanip>
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include "Utility.h"
#include "MLGGame.h"

namespace Rendering
{
	RTTI_DEFINITIONS(MouseCursorComponent)

	MouseCursorComponent::MouseCursorComponent()
		: DrawableGameComponent()
	{

	}

	MouseCursorComponent::MouseCursorComponent(Game& game, Camera& camera, Mouse& mouse)
		: DrawableGameComponent(game, camera), mMouse(&mouse), lastMousePosX(mouse.X()), lastMousePosY(mouse.Y()), mSpriteBatch(nullptr), mSpriteFont(nullptr)
	{

	}

	MouseCursorComponent::~MouseCursorComponent()
	{
		DeleteObject(mSpriteBatch);
		DeleteObject(mSpriteFont);
	}

	void MouseCursorComponent::Initialize()
	{
		SetCurrentDirectory(Utility::ExecutableDirectory().c_str());

		mSpriteBatch = new SpriteBatch(mGame->Direct3DDeviceContext());
		mSpriteFont = new SpriteFont(mGame->Direct3DDevice(), L"Content\\Fonts\\Arial_14_Regular.spritefont");
	}

	void MouseCursorComponent::Update(const GameTime& gameTime)
	{
		DrawableGameComponent::Update(gameTime);

		long xDelta = mMouse->X() - lastMousePosX;
		long yDelta = mMouse->Y() - lastMousePosY;

		lastMousePosX = mMouse->X();
		lastMousePosY = mMouse->Y();

		mPosition = XMFLOAT2(xDelta + mPosition.x, yDelta + mPosition.y);

		if (mPosition.x < 0)
		{
			mPosition.x = 0;
		}
		
		if (mPosition.y < 0)
		{
			mPosition.y = 0;
		}

		if (mPosition.x > mGame->ScreenWidth())
		{
			mPosition.x = (float)mGame->ScreenWidth();
		}
		
		if (mPosition.y > mGame->ScreenHeight())
		{
			mPosition.y = (float)mGame->ScreenHeight();
		}

		if (mMouse->IsButtonDown(MouseButtons::MouseButtonsLeft))
		{
			if ((mPosition.x > (float)mGame->ScreenWidth() - 100) && mPosition.y < 30)
			{
				static_cast<MLGGame*>(mGame)->TriggerExit = true;
			}
		}
	}

	void MouseCursorComponent::Draw(const GameTime& gameTime, bool KeepSettings)
	{
		DrawableGameComponent::Draw(gameTime);

		KeepSettings = false;

		mSpriteBatch->Begin();

		std::wostringstream fpsLabel;
		fpsLabel << "*   *   *\n*   *\n*        *\n              *";
		mSpriteFont->DrawString(mSpriteBatch, fpsLabel.str().c_str(), mPosition);

		std::wostringstream exitLabel;
		exitLabel << "EXIT!!";
		mSpriteFont->DrawString(mSpriteBatch, exitLabel.str().c_str(), XMFLOAT2((float)mGame->ScreenWidth() - 100, 0));

		mSpriteBatch->End();
	}

	void MouseCursorComponent::SetNewMouse(Mouse& mouse)
	{
		mMouse = &mouse;

		lastMousePosX = mMouse->X();
		lastMousePosY = mMouse->Y();

		mPosition = XMFLOAT2(0, 0);
	}

	Mouse* MouseCursorComponent::GetMouse()
	{
		return mMouse;
	}
}