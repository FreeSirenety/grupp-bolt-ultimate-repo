#pragma once

#include "DrawableGameComponent.h"

using namespace Library;

namespace Library
{
	class Material;
}

namespace Rendering
{
	class GameObjectComponent : public DrawableGameComponent
	{
		RTTI_DECLARATIONS(GameObjectComponent, DrawableGameComponent)

	public:
		GameObjectComponent(Game& game, Camera& camera, bool hasHP = false, int hp = 0);
		~GameObjectComponent();

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime, bool KeepSettings = false) override;

		void SetPosition(XMVECTOR position);
		void SetPosition(float x, float y, float z);

		XMVECTOR GetPosition();

		void Move(XMVECTOR movementAmount);
		void Move(float x, float y, float z);

		void SetRotation(float amount);
		void Rotate(float amount);

		const bool& GetHasHP();

		void SetHP(int hp);
		void ChangeHP(int hp);
		const int& GetHP();

		void SetScale(XMVECTOR scale);
		void SetScale(float x, float y, float z);
		XMVECTOR GetScale();

		virtual Material* CreateMaterial();
		
		void SetMaterial(Material* material);
		Material* GetMaterial();

		void SetVelocity(XMVECTOR velocity);
		XMFLOAT3 GetVelocity();

		void SetRadius(float radius);
		float GetRadius();

		virtual void Transform();

	protected:

		

		bool mHasHP;
		int mHP;
		float mRotation;
		
		XMFLOAT3 mScale;
		bool mIsDirty;

		XMFLOAT4X4 mWorldMatrix;

		Material* mMaterial;

		XMFLOAT3 mVelocity;

		XMFLOAT3 mPosition;

		float mRadius;
		
	};
}