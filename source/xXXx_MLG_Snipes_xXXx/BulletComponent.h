#pragma once

#include "GameObjectComponent.h"

using namespace Library;

namespace Library
{
	class Camera;
	class Keyboard;
	class PointLight;
	class Effect;
}

namespace Rendering
{

	class BulletMaterial;

	class BulletComponent : public GameObjectComponent
	{
		RTTI_DECLARATIONS(BulletComponent, GameObjectComponent)

	public:
		BulletComponent(Game& game, Camera& camera);
		~BulletComponent();

		void TriggerBullet(XMVECTOR position, XMVECTOR velocity, float timeToLive, float damage);

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime, bool KeepSettings = false) override;

		void SetTimeToLive(float timeToLive);
		float GetTimeToLive();

		virtual Material* CreateMaterial() override;

		virtual void Transform() override;
	private:
		float mTimeToLive;
		float mTimeCurrentlyLived;

		float mDamage;

		static const float LightModulationRate;
		static const float LightMovementRate;

		Effect* mEffect;
		ID3D11ShaderResourceView* mTextureShaderResourceView;
		ID3D11Buffer* mVertexBuffer;
		ID3D11Buffer* mIndexBuffer;
		UINT mIndexCount;

		Keyboard* mKeyboard;
		XMCOLOR mAmbientColor;
		PointLight* mPointLight;
		XMCOLOR mSpecularColor;
		float mSpecularPower;
	};
}