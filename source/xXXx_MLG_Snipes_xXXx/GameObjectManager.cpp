#include "GameObjectManager.h"
#include "EnemyComponent.h"
#include "PlayerComponent.h"
#include "Game.h"
#include "Camera.h"
#include "Material.h"


using namespace Library;


namespace Rendering
{

	RTTI_DEFINITIONS(GameObjectManager)

	GameObjectManager::GameObjectManager(Game& game, Camera& camera)
	: GameComponent(game) ,mCamera(&camera)
	{
	}

	GameObjectManager::~GameObjectManager()
	{
		for (auto materials : mObjectMaterials)
		{
			DeleteObject(materials.second);
		}

		mObjectMaterials.clear();
	}

	void GameObjectManager::Initialize()
	{

	}

	void GameObjectManager::SetMaterialForObjectType(EObjectType objectType, Material* material)
	{
		mObjectMaterials.insert(std::pair<EObjectType, Material*>(objectType, material));
	}
}