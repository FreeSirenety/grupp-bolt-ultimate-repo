#pragma once

#include "Common.h"
#include "DrawableGameComponent.h"

using namespace Library;

namespace Library
{
	class Mouse;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class MouseCursorComponent : public DrawableGameComponent
	{
		RTTI_DECLARATIONS(MouseCursorComponent, DrawableGameComponent)

	public:
		MouseCursorComponent();
		MouseCursorComponent(Game& game, Camera& camera, Mouse& mouse);

		virtual ~MouseCursorComponent();

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime, bool KeepSettings = false) override;

		void SetNewMouse(Mouse& mouse);
		Mouse* GetMouse();

	private:
		Mouse* mMouse;

		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;

		long lastMousePosX;
		long lastMousePosY;

		XMFLOAT2 mPosition;
	};
}