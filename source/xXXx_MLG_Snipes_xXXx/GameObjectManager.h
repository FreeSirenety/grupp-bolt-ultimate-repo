#pragma once

#include "GameComponent.h"
#include <vector>
#include <map>

using namespace Library;

namespace Library
{
	class Game;
	class Material;
	class Effect;
	class Camera;
}

namespace Rendering
{
	class GameObjectComponent;

	class GameObjectManager : public GameComponent
	{
		RTTI_DECLARATIONS(GameObjectManager, GameComponent)

	public:
		GameObjectManager(Game& game, Camera& camera);

		virtual ~GameObjectManager();

		virtual void Initialize();

		template <class T>
		T *CreateGameObject(EObjectType gameObjectType)
		{
			T *object = new T(*mGame, *mCamera);

			auto material = mObjectMaterials.find(gameObjectType);

			if (material == mObjectMaterials.end())
			{

				mObjectMaterials.insert(std::pair<EObjectType, Material*>(gameObjectType, object->CreateMaterial()));

				material = mObjectMaterials.find(gameObjectType);
			}

			object->SetMaterial(material->second);

			return object;
		}

		void SetMaterialForObjectType(EObjectType objectType, Material* material);
	private:
		std::map<EObjectType, Material*> mObjectMaterials;

		Camera *mCamera;
	};
}