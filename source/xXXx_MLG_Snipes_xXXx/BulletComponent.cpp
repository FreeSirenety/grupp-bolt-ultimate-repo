#include "BulletComponent.h"
#include "GameTime.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "BulletMaterial.h"
#include <DDSTextureLoader.h>
#include "GameException.h"
#include "PlayerCamera.h"
#include "PointLight.h"
#include "Game.h"

using namespace Library;

namespace Rendering
{
	RTTI_DEFINITIONS(BulletComponent)

		BulletComponent::BulletComponent(Game& game, Camera& camera)
		: GameObjectComponent(game, camera)
	{
			mPosition = XMFLOAT3(0, 0, 0);
			mScale = XMFLOAT3(0.3f, 0.3f, 0.3f);
	}

	BulletComponent::~BulletComponent()
	{
		DeleteObject(mPointLight);
		//DeleteObject(mEffect);
	}

	void BulletComponent::Initialize()
	{
		mPointLight = new PointLight(*mGame);
		mPointLight->SetRadius(500.0f);
		mPointLight->SetPosition(5.0f, 0.0f, 10.0f);
	}

	void BulletComponent::Update(const GameTime& gameTime)
	{
		GameObjectComponent::Update(gameTime);

		XMVECTOR velocity = XMLoadFloat3(&mVelocity);

		Move(velocity * (float)gameTime.ElapsedGameTime() * 30.F);

		mTimeCurrentlyLived += (float)gameTime.ElapsedGameTime();

		if (mTimeCurrentlyLived > mTimeToLive)
		{
			TriggerDestroy();
		}
	}

	void BulletComponent::Draw(const GameTime& gameTime, bool KeepSettings)
	{
		mIsDirty = true;

		GameObjectComponent::Draw(gameTime);

		BulletMaterial* material = static_cast<BulletMaterial*>(mMaterial);

		if (KeepSettings)
		{
			Pass* pass = mMaterial->CurrentTechnique()->Passes().at(0);

			XMMATRIX worldMatrix = XMLoadFloat4x4(&mWorldMatrix);
			XMMATRIX wvp = worldMatrix * mCamera->ViewMatrix() * mCamera->ProjectionMatrix();
			XMVECTOR ambientColor = XMLoadColor(&mAmbientColor);
			XMVECTOR specularColor = XMLoadColor(&mSpecularColor);



			material->WorldViewProjection() << wvp;
			material->World() << worldMatrix;
			material->SpecularColor() << specularColor;
			material->SpecularPower() << mSpecularPower;
			material->AmbientColor() << ambientColor;

			ID3D11DeviceContext* direct3DDeviceContext = mGame->Direct3DDeviceContext();

			pass->Apply(0, direct3DDeviceContext);

			direct3DDeviceContext->DrawIndexed(mMaterial->mIndexCount, 0, 0);
		}
		else
		{
			ID3D11DeviceContext* direct3DDeviceContext = mGame->Direct3DDeviceContext();
			direct3DDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			Pass* pass = mMaterial->CurrentTechnique()->Passes().at(0);
			ID3D11InputLayout* inputLayout = mMaterial->InputLayouts().at(pass);
			direct3DDeviceContext->IASetInputLayout(inputLayout);

			UINT stride = mMaterial->VertexSize();
			UINT offset = 0;
			direct3DDeviceContext->IASetVertexBuffers(0, 1, &mMaterial->mVertexBuffer, &stride, &offset);
			direct3DDeviceContext->IASetIndexBuffer(mMaterial->mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

			XMMATRIX worldMatrix = XMLoadFloat4x4(&mWorldMatrix);
			XMMATRIX wvp = worldMatrix * mCamera->ViewMatrix() * mCamera->ProjectionMatrix();
			XMVECTOR ambientColor = XMLoadColor(&mAmbientColor);
			XMVECTOR specularColor = XMLoadColor(&mSpecularColor);

			material->WorldViewProjection() << wvp;
			material->World() << worldMatrix;
			material->SpecularColor() << specularColor;
			material->SpecularPower() << mSpecularPower;
			material->AmbientColor() << ambientColor;
			material->LightColor() << mPointLight->ColorVector();
			material->LightPosition() << mPointLight->PositionVector();
			material->LightRadius() << mPointLight->Radius();
			material->ColorTexture() << material->mTextureShaderResourceView;
			material->CameraPosition() << mCamera->PositionVector();

			pass->Apply(0, direct3DDeviceContext);

			direct3DDeviceContext->DrawIndexed(mMaterial->mIndexCount, 0, 0);
		}
	}

	Material* BulletComponent::CreateMaterial()
	{
		SetCurrentDirectory(Utility::ExecutableDirectory().c_str());

		std::unique_ptr<Model> model(new Model(*mGame, "Content\\Models\\Bullet.fbx", true));


		// Initialize the material
		mEffect = new Effect(*mGame);
		mEffect->LoadCompiledEffect(L"Content\\Effects\\PointLight.cso");

		BulletMaterial* material = new BulletMaterial();
		material->Initialize(mEffect);

		Mesh* mesh = model->Meshes().at(0);
		material->CreateVertexBuffer(mGame->Direct3DDevice(), *mesh, &material->mVertexBuffer);
		mesh->CreateIndexBuffer(&material->mIndexBuffer);
		material->mIndexCount = mesh->Indices().size();

		std::wstring textureName = L"Content\\Textures\\AssignmentAdvanced_Mech_D.dds";
		HRESULT hr = DirectX::CreateDDSTextureFromFile(mGame->Direct3DDevice(), textureName.c_str(), nullptr, &material->mTextureShaderResourceView);
		if (FAILED(hr))
		{
			throw GameException("CreateDDSTextureFromFile() failed.", hr);
		}

		return material;
	}

	void BulletComponent::SetTimeToLive(float timeToLive)
	{
		mTimeToLive = timeToLive;
	}

	float BulletComponent::GetTimeToLive()
	{
		return mTimeToLive;
	}

	void BulletComponent::TriggerBullet(XMVECTOR position, XMVECTOR velocity, float timeToLive, float damage)
	{
		XMStoreFloat3(&mPosition, position);
		XMStoreFloat3(&mVelocity, velocity);
		mTimeToLive = timeToLive;
		mTimeCurrentlyLived = 0;
		mDamage = damage;
	}

	void BulletComponent::Transform()
	{
		mIsDirty = false;

		XMMATRIX world = XMMatrixIdentity() * XMMatrixScaling(mScale.x, mScale.y, mScale.z) * XMMatrixRotationX(90.f * 0.0174532925f) * XMMatrixRotationY(mRotation)  * XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z);

		XMStoreFloat4x4(&mWorldMatrix, world);
	}
}