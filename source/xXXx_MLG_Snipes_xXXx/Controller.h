#pragma once

#include "GameComponent.h"

#include "Xinput.h"

using namespace Library;

namespace Rendering
{
	class Controller : public GameComponent
	{

		RTTI_DECLARATIONS(Controller, GameComponent)

	public:
		Controller(Game& game);

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;

		XINPUT_STATE GetState();

		bool IsConnected();

		void Vibrate(int leftval = 0, int rightval = 0);

		float GetRightAngle();
		XMVECTOR GetLeftVector();

	private:
		XINPUT_STATE mControllerState;
		int mControllerNum;
	};
}