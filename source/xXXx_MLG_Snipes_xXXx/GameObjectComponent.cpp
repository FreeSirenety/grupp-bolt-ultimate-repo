#include "GameObjectComponent.h"
#include "Material.h"
#include "Camera.h"
#include "Frustum.h"
#include "MLGGame.h"

using namespace Library;

namespace Rendering
{
	RTTI_DEFINITIONS(GameObjectComponent)

	GameObjectComponent::GameObjectComponent(Game& game, Camera& camera, bool hasHP, int hp)
	: DrawableGameComponent(game, camera), mHasHP(hasHP), mHP(hp), mRotation(0), mPosition(0, 0, 0), mIsDirty(false), mVelocity(0, 0, 0)
	{

	}

	GameObjectComponent::~GameObjectComponent()
	{

	}

	void GameObjectComponent::Initialize()
	{

	}

	void GameObjectComponent::Update(const GameTime& /*gameTime*/)
	{
		if (!mVisible)
		{
			return;
		}
		Camera* camera = static_cast<Camera*>(mGame->Services().GetService(Camera::TypeIdClass()));

		if (camera != nullptr)
		{
			
			Frustum* frustum = &camera->m_frustum;

			mInside = frustum->IsInside(&mPosition, mRadius);

		}
	}

	void GameObjectComponent::Draw(const GameTime& /*gameTime*/, bool /*KeepSettings*/)
	{
		if (mIsDirty)
			Transform();

		
	}

	void GameObjectComponent::SetPosition(XMVECTOR position)
	{
		mIsDirty = true;
		XMStoreFloat3(&mPosition, position);
	}

	void GameObjectComponent::SetPosition(float x, float y, float z)
	{
		mIsDirty = true;
		XMVECTOR position = XMVECTOR();

		position = XMVectorSetX(position, x);
		position = XMVectorSetY(position, y);
		position = XMVectorSetZ(position, z);

		XMStoreFloat3(&mPosition, position);
	}

	XMVECTOR GameObjectComponent::GetPosition()
	{
		return XMLoadFloat3(&mPosition);
	}

	void GameObjectComponent::Move(XMVECTOR movementAmount)
	{
		mIsDirty = true;

		XMVECTOR position = XMLoadFloat3(&mPosition);

		position += movementAmount;

		XMStoreFloat3(&mPosition, position);
	}

	void GameObjectComponent::Move(float x, float y, float z)
	{
		mIsDirty = true;

		XMVECTOR movement = XMVECTOR();

		movement = XMVectorSetX(movement, x);
		movement = XMVectorSetY(movement, y);
		movement = XMVectorSetZ(movement, z);

		XMVECTOR position = XMLoadFloat3(&mPosition);

		position += movement;

		XMStoreFloat3(&mPosition, position);
	}

	void GameObjectComponent::SetRotation(float amount)
	{
		mIsDirty = true;
		mRotation = amount;

		/*while (mRotation > 3.14)
		{
			mRotation -= 3.14;
		}

		while (mRotation < -3.14)
		{
			mRotation += 3.14;
		}*/
	}

	void GameObjectComponent::Rotate(float amount)
	{
		mIsDirty = true;
		mRotation += amount;

		/*while (mRotation > 3.14)
		{
			mRotation -= 3.14;
		}

		while (mRotation < -3.14)
		{
			mRotation += 3.14;
		}*/
	}

	const bool& GameObjectComponent::GetHasHP()
	{
		return mHasHP;
	}

	void GameObjectComponent::SetHP(int hp)
	{
		mHP = hp;
	}

	void GameObjectComponent::ChangeHP(int hp)
	{
		mHP += hp;
	}

	const int& GameObjectComponent::GetHP()
	{
		return mHP;
	}

	void GameObjectComponent::SetScale(XMVECTOR scale)
	{
		XMStoreFloat3(&mScale, scale);
	}

	void GameObjectComponent::SetScale(float x, float y, float z)
	{
		XMVECTOR scale = XMVECTOR();

		scale = XMVectorSetX(scale, x);
		scale = XMVectorSetY(scale, y);
		scale = XMVectorSetZ(scale, z);

		XMStoreFloat3(&mScale, scale);
	}

	XMVECTOR GameObjectComponent::GetScale()
	{
		return XMLoadFloat3(&mScale);
	}

	void GameObjectComponent::Transform()
	{
		mIsDirty = false;

		XMMATRIX world = XMMatrixIdentity() * XMMatrixTranslation(0.f, 0.f, 150.f) * XMMatrixScaling(mScale.x, mScale.y, mScale.z) * XMMatrixRotationY(mRotation) * XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z); 

		XMStoreFloat4x4(&mWorldMatrix, world);
	}

	Material* GameObjectComponent::CreateMaterial()
	{
		return nullptr;
	}

	void GameObjectComponent::SetMaterial(Material* material)
	{
		mMaterial = material;
	}

	Material* GameObjectComponent::GetMaterial()
	{
		return mMaterial;
	}

	void GameObjectComponent::SetVelocity(XMVECTOR velocity)
	{
		XMStoreFloat3(&mVelocity, velocity);
	}

	XMFLOAT3 GameObjectComponent::GetVelocity()
	{
		return mVelocity;
	}

	void GameObjectComponent::SetRadius(float radius)
	{
		mRadius = radius;
	}

	float GameObjectComponent::GetRadius()
	{
		return mRadius;
	}
}