// StateManager
#include "StateManager.h"

RTTI_DEFINITIONS(StateManager)

StateManager::StateManager()
{

}

StateManager::~StateManager()
{
}


void StateManager::Initialize()
{
	mQueue = false;
	mCurrentState = STARTSTATE;
}
void StateManager::Update(const Library::GameTime& /*gameTime*/)
{
	
}

void StateManager::SetQueue(States state)
{
	mQueuedState = state;
	mQueue = true;
}

void StateManager::QueuedStateChange()
{
	if (mQueue)
	{
		mCurrentState = mQueuedState;
		mQueue = false;
	}
	return;
}
States StateManager::GetState()
{
	return mCurrentState;
}