//CircleVsCirlceCollision

#include "SoundManager.h"

struct circle
{
	float x;     
	float y;
	float radius;
};

class CircleVsCirlceCollision
{
	
	CircleVsCirlceCollision();
	~CircleVsCirlceCollision();

	public:
	int isColliding(struct circle c1, struct circle c2);


	private:
		float mDistance;
		SoundManager* mSoundmanager;
};

