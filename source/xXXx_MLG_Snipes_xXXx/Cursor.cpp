#include "Cursor.h"
#include "EnemyMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "PointLight.h"
#include "Keyboard.h"
#include "Mouse.h"
#include <DDSTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>
#include "SoundManager.h"
#include "GameObjectComponent.h"
#include <math.h>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

namespace Rendering
{
	RTTI_DEFINITIONS(Cursor)

		const float Cursor::LightModulationRate = UCHAR_MAX;
	const float Cursor::LightMovementRate = 10.0f;

	Cursor::Cursor(Game& game, Camera& camera)
		: GameObjectComponent(game, camera), mEffect(nullptr), mTextureShaderResourceView(nullptr),
		mVertexBuffer(nullptr), mIndexBuffer(nullptr), mIndexCount(0),
		mKeyboard(nullptr), mAmbientColor(1, 1, 1, 0), mPointLight(nullptr),
		mSpecularColor(1.0f, 1.0f, 1.0f, 1.0f), mSpecularPower(25.0f), mProxyModel(nullptr),
		mRenderStateHelper(nullptr), mSpriteBatch(nullptr), mSpriteFont(nullptr), mTextPosition(0.0f, 40.0f)
	{
	}

	Cursor::~Cursor()
	{
		DeleteObject(mEffect);
		DeleteObject(mPointLight);
		DeleteObject(mProxyModel);
		DeleteObject(mRenderStateHelper);
		DeleteObject(mSpriteBatch);
		DeleteObject(mSpriteFont);
	}

	void Cursor::Initialize()
	{
		mPosition = Vector3Helper::Zero;

		srand((unsigned int)time(nullptr));

		XMStoreFloat4x4(&mWorldMatrix, XMMatrixScaling(0.03f, 0.03f, 0.03f));

		mPointLight = new PointLight(*mGame);
		mPointLight->SetRadius(500.0f);
		mPointLight->SetPosition(5.0f, 0.0f, 10.0f);

		mKeyboard = (Keyboard*)mGame->Services().GetService(Keyboard::TypeIdClass());
		assert(mKeyboard != nullptr);

		mMouse = (Mouse*)mGame->Services().GetService(Mouse::TypeIdClass());

		mProxyModel = new ProxyModel(*mGame, *mCamera, "Content\\Models\\PointLightProxy.obj", 0.5f);
		mProxyModel->Initialize();

		mRenderStateHelper = new RenderStateHelper(*mGame);

		mSpriteBatch = new SpriteBatch(mGame->Direct3DDeviceContext());
		mSpriteFont = new SpriteFont(mGame->Direct3DDevice(), L"Content\\Fonts\\Arial_14_Regular.spritefont");

		SetScale(0.01f, 0.01f, 0.01f);

		mPlayer = (PlayerComponent*)mGame->Services().GetService(PlayerComponent::TypeIdClass());

		
		return;
	}

	void Cursor::Update(const GameTime& gameTime)
	{

		mProxyModel->Update(gameTime);
		

		HandleManualMovement(gameTime);

	}
	void Cursor::HandleManualMovement(const GameTime& gameTime)
	{
		if (mKeyboard->IsKeyDown(DIK_I))
		{
			Move(0, 0, -10 * (float)gameTime.ElapsedGameTime());

		}

		if (mKeyboard->IsKeyDown(DIK_K))
		{
			mStatemanager->QueuedStateChange();
			Move(0, 0, 10 * (float)gameTime.ElapsedGameTime());
		}

		if (mKeyboard->IsKeyDown(DIK_J))
		{
			Move(-10 * (float)gameTime.ElapsedGameTime(), 0, 0);
		}

		if (mKeyboard->IsKeyDown(DIK_L))
		{
			Move(10 * (float)gameTime.ElapsedGameTime(), 0, 0);
		}
	}
	

	void Cursor::Draw(const GameTime& gameTime, bool KeepSettings)
	{
		GameObjectComponent::Draw(gameTime);

		EnemyMaterial* material = static_cast<EnemyMaterial*>(mMaterial);

		if (KeepSettings)
		{
			Pass* pass = mMaterial->CurrentTechnique()->Passes().at(0);

			XMMATRIX worldMatrix = XMLoadFloat4x4(&mWorldMatrix);
			XMMATRIX wvp = worldMatrix * mCamera->ViewMatrix() * mCamera->ProjectionMatrix();
			XMVECTOR ambientColor = XMLoadColor(&mAmbientColor);
			XMVECTOR specularColor = XMLoadColor(&mSpecularColor);



			material->WorldViewProjection() << wvp;
			material->World() << worldMatrix;
			material->SpecularColor() << specularColor;
			material->SpecularPower() << mSpecularPower;
			material->AmbientColor() << ambientColor;

			ID3D11DeviceContext* direct3DDeviceContext = mGame->Direct3DDeviceContext();

			pass->Apply(0, direct3DDeviceContext);

			direct3DDeviceContext->DrawIndexed(mMaterial->mIndexCount, 0, 0);
		}
		else
		{
			ID3D11DeviceContext* direct3DDeviceContext = mGame->Direct3DDeviceContext();
			direct3DDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			Pass* pass = mMaterial->CurrentTechnique()->Passes().at(0);
			ID3D11InputLayout* inputLayout = mMaterial->InputLayouts().at(pass);
			direct3DDeviceContext->IASetInputLayout(inputLayout);

			UINT stride = mMaterial->VertexSize();
			UINT offset = 0;
			direct3DDeviceContext->IASetVertexBuffers(0, 1, &mMaterial->mVertexBuffer, &stride, &offset);
			direct3DDeviceContext->IASetIndexBuffer(mMaterial->mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

			XMMATRIX worldMatrix = XMLoadFloat4x4(&mWorldMatrix);
			XMMATRIX wvp = worldMatrix * mCamera->ViewMatrix() * mCamera->ProjectionMatrix();
			XMVECTOR ambientColor = XMLoadColor(&mAmbientColor);
			XMVECTOR specularColor = XMLoadColor(&mSpecularColor);

			material->WorldViewProjection() << wvp;
			material->World() << worldMatrix;
			material->SpecularColor() << specularColor;
			material->SpecularPower() << mSpecularPower;
			material->AmbientColor() << ambientColor;
			material->LightColor() << mPointLight->ColorVector();
			material->LightPosition() << mPointLight->PositionVector();
			material->LightRadius() << mPointLight->Radius();
			material->ColorTexture() << material->mTextureShaderResourceView;
			material->CameraPosition() << mCamera->PositionVector();

			pass->Apply(0, direct3DDeviceContext);

			direct3DDeviceContext->DrawIndexed(mMaterial->mIndexCount, 0, 0);
		}
	}

	Material* Cursor::CreateMaterial()
	{
		SetCurrentDirectory(Utility::ExecutableDirectory().c_str());

		std::unique_ptr<Model> model(new Model(*mGame, "Content\\Models\\Mech_Theo.obj", true));

		mPosition = Vector3Helper::Zero;


		////// Initialize the material

		mEffect = new Effect(*mGame);
		mEffect->LoadCompiledEffect(L"Content\\Effects\\PointLight.cso");

		EnemyMaterial* material = new EnemyMaterial();
		material->Initialize(mEffect);

		Mesh* mesh = model->Meshes().at(0);
		material->CreateVertexBuffer(mGame->Direct3DDevice(), *mesh, &material->mVertexBuffer);
		mesh->CreateIndexBuffer(&material->mIndexBuffer);
		material->mIndexCount = mesh->Indices().size();

		std::wstring textureName = L"Content\\Textures\\AssignmentAdvanced_Mech_D.dds";
		HRESULT hr = DirectX::CreateDDSTextureFromFile(mGame->Direct3DDevice(), textureName.c_str(), nullptr, &material->mTextureShaderResourceView);
		if (FAILED(hr))
		{
			throw GameException("CreateDDSTextureFromFile() failed.", hr);
		}

		return material;

	}
}

