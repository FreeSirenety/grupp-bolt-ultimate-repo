#include "PlayerCamera.h"
#include "Game.h"
#include "GameTime.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "VectorHelper.h"
#include "PlayerComponent.h"


namespace Rendering
{
	RTTI_DEFINITIONS(PlayerCamera)

		PlayerCamera::PlayerCamera(Game& game, PlayerComponent* playerComponent)
		: Camera(game), mPlayer(playerComponent)
	{

	}

	PlayerCamera::PlayerCamera(Game& game, PlayerComponent* playerComponent, float fieldOfView, float aspectRatio, float nearPlaneDistance, float farPlaneDistance)
		: Camera(game, fieldOfView, aspectRatio, nearPlaneDistance, farPlaneDistance), mPlayer(playerComponent)

	{

	}

	PlayerCamera::~PlayerCamera()
	{
		mPlayer = nullptr;
	}

	void PlayerCamera::Initialize()
	{
		Camera::Initialize();

		XMFLOAT2 rotationAmount = Vector2Helper::Zero;

		rotationAmount.y = -90 * 0.0174532925f;

		XMVECTOR rotationVector = XMLoadFloat2(&rotationAmount);

		XMVECTOR right = XMLoadFloat3(&mRight);

		XMMATRIX pitchMatrix = XMMatrixRotationAxis(right, XMVectorGetY(rotationVector));

		ApplyRotation(pitchMatrix);
	}

	void PlayerCamera::Update(const GameTime& gameTime)
	{
		XMVECTOR position = mPlayer->GetPosition();

		position = XMVectorSetY(position, XMVectorGetY(position) + 90.f);

		//position = XMVectorSetX(position, XMVectorGetX(position) / 1);

		XMStoreFloat3(&mPosition, position);

		Camera::Update(gameTime);
	}

	void PlayerCamera::SetPlayerComponent(PlayerComponent* playerComponent)
	{
		mPlayer = playerComponent;
	}

	PlayerComponent* PlayerCamera::GetPlayerComponent()
	{
		return mPlayer;
	}
}
