//State.h
#include "GameComponent.h"
#include "Common.h"
#include "StateManager.h"
#include "GameTime.h"


#pragma once

class State : public Library::GameComponent
{


public:
	virtual ~State();

	virtual bool Enter() = 0;
	virtual void Exit() = 0;
	virtual void Pause() = 0;
	virtual void Resume() = 0;
	virtual void Update(const Library::GameTime& gameTime) override = 0;
	virtual void Draw() = 0;
	virtual std::string Next() = 0;
	virtual bool IsType(States state) = 0;
	virtual bool isPaused() = 0;
	virtual bool isBase() = 0;

private:
	
};